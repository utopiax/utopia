/**
 * View Models used by Spring MVC REST controllers.
 */
package xyz.matar.utopia.web.rest.vm;
